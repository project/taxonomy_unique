<?php

namespace Drupal\taxonomy_unique;

/**
 * Taxonomy unique Constants.
 */
class TaxonomyUniqueConstants {

  const NOT_UNIQUE_DEFAULT_ERROR_MESSAGE = 'Term "%term" already exists in vocabulary "%vocabulary".';

}
