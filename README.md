# Taxonomy unique

## Contents of the file

- Description
- Features
- Installation
- Configuration
- Maintainers

## Description

By default, Drupal allows creation of identical terms in the same vocabulary
Taxonomy unique prohibits saving a taxonomy term when a term with the same name
exists in the same vocabulary.
You can configure it individually for each vocabulary, and you can set custom
error messages if a duplicate is found.

## Features
- Ensures that term names are unique
- Configurable individually for each vocabulary
- Set custom error messages if a duplicate is found

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Download the module from drupal.org.
1. Untar the archive into your modules directory.
1. Go to Admin > Modules and enable the Taxonomy unique module.
1. Configure settings at Admin > Structure > Taxonomy > Vocabulary settings page

## Maintainers

- Julian Pustkuchen - [Anybody](https://www.drupal.org/u/anybody)
- Rafael Schally - [sch4lly](https://www.drupal.org/u/sch4lly)
- Joshua Sedler - [Grevil](https://www.drupal.org/u/grevil)
